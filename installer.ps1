<#Configure this section before using the script#>
param
(
	[string]$siteName = "$(Read-Host 'Website Name[e.g. BookingEasy]')", 
	[string]$property = "$siteName-pool", 
	[switch]$verbose = $true,
	[switch]$debug = $false,
	[string]$BepozDir = "$env:Bepoz",
	[string]$net40Path = [System.IO.Path]::Combine($env:SystemRoot, "Microsoft.NET\Framework\v4.0.30319"),
    [string]$netFullName = [System.IO.Path]::Combine($net40Path, "System.dll"),
	[string]$sqlcom35reg = 'HKLM:SOFTWARE\Microsoft\Microsoft SQL Server Compact Edition\v3.5',
	[string]$visualc = 'HKLM:SOFTWARE\Microsoft\VisualStudio\10.0\VC\VCRedist\',
    [string]$visx64 = Combine($visual, "x64"),
    [string]$visia64 = Combine($visual, "ia64"),
    [string]$visx86 = Combine($visual, "x86"),
	[string]$OSBitVersion = "$(Read-Host 'Enter in the OS Version x86,x64,ai64')"
	
)
write-host "Enter in the requested fields to continue the Install" -foregroundcolor yellow;

$Boxstarter.RebootOk=$true # Allow reboots?
$Boxstarter.NoPassword=$false # Is this a machine with no login password?
$Boxstarter.AutoLogin=$true # Save my password securely and auto-login after a reboot

# Starter Enviorment Varibles
write-host "Changing the Basic Enviorment Varibles for instller to run" -foregroundcolor green;
function main() {
Update-ExecutionPolicy Unrestricted #Alows for Powershell Scripts to Run on the machine !important
Set-ExplorerOptions -showHidenFilesFoldersDrives -showProtectedOSFiles -showFileExtensions #Folder permission setting in the windows enviorment
Enable-RemoteDesktop # Allows for remote Desktop insttances
Disable-InternetExplorerESC #
Disable-UAC # Disables UAC
Set-TaskbarSmall # decreases the Tasbar size to Small is anything else
}

function WinUpdate (){
if (Test-PendingReboot) { Invoke-Reboot }

# Update Windows and reboot if necessary
Install-WindowsUpdate -AcceptEula -Full
if (Test-PendingReboot) { Invoke-Reboot }
}


function DotNet ($netFullName ){
    if ((test-path $netFullName) -eq $false)
    {
        $message =  "net 4.0 > was not found in {0}. Make sure Microsoft .NET Framework 4.5.2 installed first." -f $net40Path;
        write-error $message;
        cinstm DotNet4.5.2 # Install the needed Dot Net framework for the Bepoz software to fun. Uses Windows Features to install.
		if (Test-PendingReboot) { Invoke-Reboot } # Alows the computer to reboot if needed after installation
    }
 
    write-host "Microsoft .NET Framework 4.0 appears to be installed." -foregroundcolor green;
}

function SqlCompact ($sqlcom35reg){
if ((Test-Path $sqlcom35reg) -eq $false){
        $message =  "Sql Server Compact v3.5 was not found in {0}";
        write-error $message;
		cinstm mssqlserver-compact3.5
		if (Test-PendingReboot) { Invoke-Reboot } # Alows the computer to reboot if needed after installation
}
  write-host "Sql Server Compact v3.5 appeara to be installed" -foregorundcolor green;
}

function VisualC (){
if ((Test-Path $visx64) -eq $false)
{
$message = "Microsoft Visual C++ 2010 x64 was not found to be installed." -foregorundcolor yellow;
write-error $message;
cinstm vcredist2010
if (Test-PendingReboot) { Invoke-Reboot } # Alows the computer to reboot if needed after installation
}
}
function WinDep (){
cinst IIS-WebServerManagementTools -source windowsfeatures

folders

}
#Other essential tools
function folders ($BepozDir){
#Testing and making the core Bepoz folder structure
    if(!(Test-Path "$BepozDir")){mkdir "$BepozDir"}
    if(!(Test-Path "$BepozDir")){mkdir "$BepozDir\Backup"}
    if(!(Test-Path "$BepozDir")){mkdir "$BepozDir\Data"}
    if(!(Test-Path "$BepozDir")){mkdir "$BepozDir\Manuals"}
    if(!(Test-Path "$BepozDir")){mkdir "$BepozDir\NewVersion"}
    if(!(Test-Path "$BepozDir")){mkdir "$BepozDir\OldVersions"}
    if(!(Test-Path "$BepozDir")){mkdir "$BepozDir\Programs"}
    if(!(Test-Path "$BepozDir")){mkdir "$BepozDir\Version"}
#Testing and making the Extras folder for Bepoz America
  $BepozExtrasDir = "$env:Bepoz\Extras"
    if(!(Test-Path "$BepozExtrasDir")){mkdir "$BepozExtrasDir"}
    if(!(Test-Path "$BepozExtrasDir")){mkdir "$BepozExtrasDir\Sample_Databases"}
    if(!(Test-Path "$BepozExtrasDir")){mkdir "$BepozExtrasDir\Backdrops"}
    if(!(Test-Path "$BepozExtrasDir")){mkdir "$BepozExtrasDir\Vendor_Drivers"}
    if(!(Test-Path "$BepozExtrasDir")){mkdir "$BepozExtrasDir\Support"}
#Testing and making sure the Bepoz folder is on the desktop
  $BepozExtrasDir = "$env:\Extras"
  }
function firewallA (){
#Firewall Rules
netsh advfirewall firewall add rule name="Bepoz Smart Controller" dir=in action=allow protocol=TCP localport=19800-19820 
netsh advfirewall firewall add rule name="Bepoz Voucher Print" dir=in action=allow protocol=TCP localport=12006 
netsh advfirewall firewall add rule name="Bepoz SmartController.exe" dir=in action=allow program="C:\bepoz\programs\smartcontrol.exe" 
netsh advfirewall firewall add rule name="Bepoz Database.exe" dir=in action=allow program="C:\bepoz\programs\database.exe" 
netsh advfirewall firewall add rule name="Bepoz BackOffice.exe" dir=in action=allow program="C:\bepoz\programs\backoffice.exe" 
netsh advfirewall firewall add rule name="Bepoz SmartPOS.exe" dir=in action=allow program="C:\bepoz\programs\smartpos.exe" 
netsh advfirewall firewall add rule name="Bepoz SmartPrint32.exe" dir=in action=allow program="C:\bepoz\programs\smartprint32.exe" 
reg add "HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\Terminal Server" /v fDenyTSConnections /t REG_DWORD /d 0 /f 
netsh advfirewall firewall set rule group="remote desktop" new enable=Yes 
netsh advfirewall firewall add rule name="All ICMP V4" dir=in action=allow protocol=icmpv4 
netsh advfirewall firewall add rule name="Bepoz API Server Connection" dir=in action=allow protocol=TCP localport=56200
netsh advfirewall firewall add rule name="Bepoz API Server Connection" dir=in action=allow protocol=TCP localport=53100
}
function Powerplan (){
# Changing the Power Saving Settings
$MyPlan= �High Performance�
Write-Host �Setting Power Plan to $MyPlan�
$guid = (Get-WmiObject -Class win32_powerplan -Namespace rootcimv2power -Filter �ElementName=�$MyPlan'�).InstanceID.tostring()
$regex = [regex]�{(.*?)}$�
$newPower = $regex.Match($guid).groups[1].value
powercfg -S $newPower
Write-Host �Setting Standby Timeout to Never�
POWERCFG -change -standby-timeout-ac 0
Write-Host �Setting Monitor Timeout to Never�
POWERCFG -change -monitor-timeout-ac 0
}
function shortcuts (){
#creating short cuts
Install-ChocolateyPinnedTaskBarItem "$($Boxstarter.programFiles86)\PathToApplication.exe"
Install-ChocolateyPinnedTaskBarItem "$($Boxstarter.programFiles86)\PathtoApplication.exe"
}
function activedrives (){
Get-WmiObject -Class Win32_LogicalDisk | Where { $_.DriveType -eq 3 }
}
